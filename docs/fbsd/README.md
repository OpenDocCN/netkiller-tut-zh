# Netkiller FreeBSD 手札

### netkiller Neo Chan

`<netkiller(at)msn(dot)commsn(dot)com)>`

版权 © 2008-2017 Copyright netkiller

**版权声明**

转载请与作者联系，转载时请务必标明文章原始出处和作者信息及本声明。

|  &#124; ![ &#124;](http://creativecommons.org/licenses/by/3.0/)  |  

&#124; 文档出处: &#124;
&#124; [`netkiller.github.io`](http://netkiller.github.io/) &#124;
&#124; [`netkiller.sourceforge.net`](http://netkiller.sourceforge.net/) &#124;

 |  &#124; ![ &#124;](img/weixin.jpg)  | 微信扫描二维码进入 Netkiller 微信订阅号 QQ 群：128659835 请注明“读者” |

* * *