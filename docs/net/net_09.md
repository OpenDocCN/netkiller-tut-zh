# 第 29 章 IBM

## IMM

要访问 Web 界面，您需要可通过 Setup Utility 获取的 IMM IP 地址。要获取该 IP 地 址，请完成以下步骤：

```

1\. 开启服务器。
2\. 当显示<F1> Setup 提示时，请按 F1 键。如果您设置了开机密码和管理员密码，
那么必须输入管理员密码才能访问完整的 Setup Utility 菜单。
3\. 选择 System Settings → Integrated Management Module → Network Configuration。
4\. 找到该 IP 地址。
5\. 退出 Setup Utility。	

```

注：最初设置的 IMM 用户名为 USERID，密码为 PASSW0RD（其中的“0”是数 字“零”，而不是字母“O”）。您具有读/写访问权。当您第一次登录时，必须更 改缺省密码。