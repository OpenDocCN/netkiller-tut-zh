# 第 16 章 产品管理

产品经理应该考虑的不是为设计一款产品卖给客户，而是帮助他们解决问题

## 1. 需求

客户自身的需求是一切购买行为的源头。

如果一家企业，能够捕捉到客户自己都没有意识到的需求，那么它往往可以改变世界。

### 1.1. 需求心理

用户的需求心理，由内而外：内心愿望，问题和困惑，解决方案，采购标准，产品和服务。

无论是产品人员还是市场人员，对需求能理解到什么层次，体现了思维处于什么水平，同是决定了销售的结果或用户体验。

### 1.2. 消费者思维阶段

感性认识，理性认识，感性决策，理性决策。

### 1.3. 需求的挖掘

需求采集、需求分析、需求筛选、需求处理

## 2. 谈设计

今天谈谈设计这个话题。

### 2.1. 为什么中国设计如此“丑”

从工业产品，包装，网站，游戏，建筑，园林......

不是中国没有好的设计师，而是最终决定权不在设计师，而是审美较差的产品经理或企业高层。

我们常常会听到管理层说一句话：“不好看”，但有无法说清楚为什么不好看，只回复你一句“改”。

通常是设计师会按照上面的意愿修改设计，最终的结果是越来越丑，甚至一句话气死设计师：“还是第一版好看”

在老板的眼里，别人家的设计师永远比自己的设计师强。常常让自己的设计师参考其他公司的设计，可笑的就是 A 公司老帮让设计师参考 B 公司设计，B 公司的老板让设计师参考 A 公司的设计。所以中国的设计天下一大抄。

上面的故事适合所有设计领域吧？

### 2.2. 不喜欢艺术的人正在从事艺术工作

在中国每年有大量的人参加艺考，参加艺考并不是他们喜欢艺术，而是为了文凭，相对于 985，211 等大学，艺术院校的分数线更低。

另一方面教育产业化，对于生源来者不拒，艺术院校的门槛本来是非常高的，因为这是一个靠份的领域，无法后天努力达成。

这样就造成了每年有大量从艺术院校毕业的学生踏入社会，这些不喜欢艺术的人踏入社会为了饭碗，首选当然是本专业的做工，从事艺术工作。

在招聘过程中，也遇到很多麻烦，因为艺术领域无法像其他领域那样可以通过笔试或面试来发现人才（例如考题或询问工作经验）

### 2.3. 什么是不好看？不好看的标准是什么？

好不好看我认为主要有几个因素

1.  构图比例

2.  配色

3.  风格

4.  结构合理

5.  用户体验

构图比例失调，肯定不好看；配色不合理也不好看；结构不合理，设计出来生产不出来也没有用，例如手机设计要考虑内部电路板，电池等等因素，外形不能随心所欲的设计。

你说设计不好看，说出为什么不好看

### 2.4. 在企业中数设计岗位最没地位

用个段子形容 “设计这个领域就像个婊子，谁有权力，谁就可以弄一下”。

设计作品谁都可以指点，来上一句“不好看”。

设计师没有最终定稿权，很多企业将设计这块挂在产品或者其他部门之下。

### 2.5. 艺术的欣赏是需要后天训练

对于艺术的欣赏需要后天训练，需要你不断的学习，才能等的艺术欣赏艺术，为什么需要后天训练，因为我们的教育很失败。

我想欣赏文学作品对于每个人都没有难度，因为我们的教育中“语文”占了很大比重。而美术核音乐常常被其他课时挤占，画几幅画，学几首歌就这样度过的。

### 2.6. 设计师怎样保护自己不被玩死

怎样才能让上面尽快拍板？怎样才能避免无意义的修改？

你可以提供两到三份设计稿出来，分别用心设计的最终稿，其他几分随便设计，三份形成对比分别是上，中，下。然后将三份设计稿交上去，让他们选择其中一份。这种方法能够一定程度避免被玩死。

## 3. 产设计人员常犯的错误

### 3.1. 花 80%时间开发使用率不到 20%的功能

故事一：网站页面有一个处需要每个月改动一次，产品设计人员提出需要在后台增加一个功能，用来设置该功能。

故事二：产品设计人员要求很多控制开关都做在后台，可以开启与禁用很多功能，这些功能一周，甚至一个月都不会用到一次。

我认为有很多功能一个月改动一次，甚至一年才会变动一次，完全没有必要做成动态功能，如果把这些不必要的功能都做成动态的还会影响页面载入速度。

### 3.2. 伪需求

什么是伪需求？简言之：看似有这种需求，但其实这种需求是经不起使用频次，用户体验及使用依附性的考验。看似是需求，实则是伪需求。

“伪需求”不仅在各创业公司的商业模式里较为常见，而且在大公司里随处可见。

产生“伪需求”的因是什么呢？

1.  为了需求而需求，这是产品人员的工作，当一天和尚，要念一本经

2.  将解决方案当成需求

3.  认为自己的需求就是市场需求

4.  解决了一个需求，却会影响另一个需求的需求

5.  将已经成熟的市场检验跨界移植到未知目标市场

6.  不符合用户习惯的需求

7.  无法超越现有竞品的需求

### 3.3. 未进行充分的可行性调研

头脑风暴出的 good idea，容易让人头脑发热，往往密闭认得的双眼，让人失去基本的判断力，仓促中决策，没有突入足够的时间做市场调用。

### 3.4. 乐观估算市场规模及销售额

### 3.5. 过分追求成长速度和维度

任何事物都有它的规律，成长是一个必经的过程，急于求成，疯狂扩张，揠苗助长，只能物极必反。

### 3.6. 缺乏风险控制及止损策略

计划没有变化快，举两个例子，毛泽东与蒋介石。

毛泽东擅长战略规划，至于战术交给指战员自由发挥，指战员根据战事进展随机应变。

蒋介石的作战计划非常之详细，但战事总是事与愿违，不能按照他预想的战况发展，随时时间的推移，最终无法把控。

## 4. 未来的产品设计，一切皆推送

最近接触了几个传统企业转型的项目，几个项目大同小异，我选择其中一个为例子。

这是一个建筑装潢的项目，他们做了一个采购平台，打算将采购从线下搬到线上，这样做的目的主要是，一来规范公司的采购流程，加快项目进度，减少采购过程中出现的腐败，二来是计划打造一个采购平台，让材料供应商入主平台，从入住或交易中抽成，寻找盈利模式。

我认为这样的设计初衷就是错误的。首先这种模式是上一个互联网时代的产物，即“平台+收费”的模式。其次切入点也是错误的，平台是个材料供应商交易市场。这个平台的运营模式是，第一步公司的邀请材料供应商入住平台，并收取一定费用，第二步公司的项目采购员去平台上线上采购。

你认为这个模式能成功吗？

这种将线下操作模式直接搬到线上的方法，算不上“企业数字化转型”，这个平台只是帮助采购员做了信息收集的工作。材料供应商在平台上的报价不是最低价格，通常是市场价，最终采购员仍然要一家一家谈核实价格。每种材料有多重规格，上传数据对于供应商来说工作量也不小。我们再看这个平台，它更像是企业 ERP 系统，给 ERP 系统开了一个对外入口，允许供应商上传他们的产品。

### 4.1. 互联网正在发生的变化

让我们认真回顾一下最近十年互联网商业环境发生了巨大的变化，我们经历了产品为王的时代，这个时代我们常常把教育用户，培养用户使用习惯挂在最边上。如今是我们做用户画像，分析用户群体，揣测用户兴趣爱好……

你还在使用搜索引擎吗？你最近一次使用搜索引擎是什么时候？我几乎不用搜索引擎了，APP/小程序的时代搜索引擎被弱化，从百度市值也能看出搜索引擎的时代过去了。

最近几年的你会发现互联网产品具备几个新特征：

1.  系统比你还懂得你，知道你喜欢什么
2.  不再需要你去主动获得信息，而是被动等待信息推送给你
3.  电脑端过渡到移动端

首先是你使用搜索引擎的次数越来越少，你不不再去平台花很长时间去找商品，做比较，而是从系统推送的信息中二次过滤。

然后是家里的电脑开机次数越来越少，对于很多家庭电脑不再是必需品，人们对移动设备的依赖程度越来越高，移动设备能随时随地拿出来使用，使用时间越来越长，产生的流量是电脑的几十倍，甚至很多人不止一台移动设备。

最后是使用习惯也在从“键盘+鼠标”向“触屏”方向过渡，由于移动设备没有键盘和鼠标，发展出一套特有的操作模式，使 APP 更便捷，更易于学习。电脑时代我们需要经过专业培训才能熟练操作软件，而 APP 时代是零学习成本。

来自用户的变化：

1.  用户没有耐心看完一屏以上的文字，一分钟以上的短视频，超过 30 秒音频。
2.  用户不再发表评论表达观点
3.  用户等待你喂给他信息
4.  用户忠诚度下降，随时不再关注你
5.  获得点赞都很困难

你的产品必须足够吸引用户，持续不断的喂给用户信息，还要保持信息的质量，稍有下降用户就会流失，移动互联网时代键盘侠都在消亡，用户不再发表看法，坚持自己的立场，他会佛系地划过屏幕。

所以我们设计产品应该遵循以上几个原则，一是通过学习用户行为，可以知道用户对那些内容感兴趣。二尽量做到让用户被动获得信息，即喂给用户信息。三是操作尽量简洁，动动手指即可完成交易。能用一只收，一个手指，绝不用两只手完成。

### 4.2. 我们应该怎么做这个平台？

#### 4.2.1. 采购平台变成投标平台

我换个角度，将这个平台从采购变成招标平台如何？

我们将招标信息发布不平台上，附上采购清单（内涵材料，规格，标准，数量）。材料供应商根据采购单投标，未开标前材料供应商之间的价格保密，这样采销供应商才会出底线价。开标后，公示采购价格，其他供应商看到价格后，会在下次投标中给出更合理的价格。

#### 4.2.2. 撮合模式

撮合引擎将自动匹配采购单和材料供应商，将有可能产生交易的采购和供应商筛选出来，做好匹配好，运营人员可以实时掌握匹配情况。

推荐系统也会通过消息推送，将匹配的数据推送给项目方和材料供应商。

项目方登录系统后，可以看到撮合引擎已经匹配好材料供应商。

材料供应商登录后，可以看到可能产生交易的项目采购单。

这样免去了他们相互寻找对方，撮合系统已经帮他们做了牵线搭桥。

#### 4.2.3. 机器学习

对于项目方，通过历史交易，以及采购人员的操作行为分析，可以得出，他经常采购的产品，经常打交道的供应商。

对于材料供应商，通过历史交易，以及投标好物，也能得出他们对那些项目更感兴趣。

这些数据都可以用于撮合引擎，做精准的推荐。

### 4.3. 一切皆推送

这是一个消息推送的时代，没有人在去主动获得信息，你必须将准备好的信息喂给用户。

### 4.4. APP 将取代 H5 成为主流

智能手机，平板电脑，穿戴设备，甚至部分笔记本电脑，都携带摄像头，麦克风，GPS 定位，三轴电磁罗盘，3D 面部传感器，指纹，NFC 近场通信，红外线，蓝牙，加速度传感器等等。

人们对移动设备的依赖程度越来越高，使用时间越来越长，产生的流量是 PC 的几十倍，甚至很多人不止一台移动设备。

近年来互联网的发展从 B/S 模式慢慢重回 C/S 模式，即 APP 和服务器模式。早期互联网是 C/S 模式客户端与服务器模式，由于初期需求不明确，在摸索中前行，导致客户端频繁更新，影响用户体验。

浏览器的出现解决了客户端频繁更新的问题，这就是 B/S 结构，一时间几乎所有软件企业抛弃了 C/S 接口，转向 B/S 结构，并把应用搬到云端。

iPhone 的出现，乔布斯提出了 APP 概念，APP Store 解决传统 C/S 结构软件的安装卸载和更新升级的问题，真正做到了一键安装，一键卸载，自动升级。

成就 APP 的还有 Restful API 和 JSON 数据格式，与传统 C/S 结构的软件相比，传统采用 TCP 协议通信，私有协议，没有统一标准，需要客户端与服务器长连接，而 Restful 是机遇 HTTPS 的无状态协议。

H5 即 HTML5 是 HTML4 的升级版，H5 中增加很多新特写，例如多媒体的持之，也支持 GPS 定位获取等等，但是更多传感器仍然支持有限。

H5 与 APP 相比较，H5 需要传输大量的 HTML 标记语言，而 APP 的 UI 是在客户端，与服务器之间只有 Restful 的 JSON 数据传输，用户体验上 APP 效果更好。虽然 H5 可以通过 CDN 以及本地缓存技术解决页面展现用户体验，但是缓存带来另一个问题就是，当系统升级的时候，客户端可能无法第一时间获得新的页面，甚至一部分更新，另一部分缓存没更新导致 UI 崩溃。除此之外由于网络质量不好，导致页面加载不全，H5 无法运行或出现异常。

目前主流的做法是移动 APP + H5 后台，即移动端采用 APP 开发，或安卓，或 iOS，或混合开发，例如 flutter。系统后台仍然使用 H5 开发，即网页后台。

我最近的想法是，后台也用 APP 开发，一种是 Window APP，另一种是平板电脑 APP。为什么我想使用 APP 替代网页端后台呢，因为平板电脑上的传感器可以做很多特俗的需求。例如：例如发布一个商品，可以使用蓝牙键盘连接平板电脑输入文字，上传照片直接使用摄像头拍摄。再例如审核操作，我们可以利用电容屏，让用户签名，刷脸，刷指纹确认等等。还能记录特写特定操作时 GPS 所处的位置等等。这些需求是 H5 难以实现或实现不了，即使实现用户体验不一定好。

未来办公不一定非得做到办公室里，会有越来越多的公司采用远程办公，移动办公，走动式管理等等。我做了几个选型，移动办公可以使用 Apple iPad, Microsoft Surface，Android 平板，办公室里的员工可以使用 Android X86 (安装在 PC 电脑上的安卓系统) 非常适合后台系统 APP。