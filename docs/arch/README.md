# Netkiller Architect 手札

## Multi-dimension Architecture Design & Implementation / Full Stack Architect

### Mr. Neo Chan, 陈景峯(BG7NYT)

中国广东省深圳市望海路半岛城邦三期
518067
+86 13113668890

`<netkiller@msn.com>`

MMDVM Hotspot:   YSF80337 - CN China 1 - W24166/TG46001 BM_China_46001 - DMR Radio ID 4600441 

文档尚未完成，请勿转载！

版权 © 2008-2018 Copyright Editor Groups, All Rights Reserved

**版权声明**

转载请与作者联系，转载时请务必标明文章原始出处和作者信息及本声明。

|  &#124; ![ &#124;](http://creativecommons.org/licenses/by/3.0/)  |  

&#124; [`www.netkiller.cn`](http://www.netkiller.cn) &#124;
&#124; [`netkiller.github.io`](http://netkiller.github.io/) &#124;
&#124; [`netkiller.sourceforge.net`](http://netkiller.sourceforge.net/) &#124;

 |  &#124; ![ &#124;](img/weixin.jpg)  |  

&#124; 微信订阅号 netkiller-ebook (微信扫描二维码） &#124;
&#124; QQ：13721218 请注明“读者” &#124;
&#124; QQ 群：128659835 请注明“读者” &#124;

 |

$Date: 2013-02-04 09:33:18 +0800 (Mon, 04 Feb 2013) $

* * *

| **修订历史** |
| 修订 0.1.1 | Sep 12, 2011 | Neo |
| 章节做了大调整，将文档分为五块，多维度架构，开发，运维，SQA，还有 DevOps。 |
| 修订 0.1.0 | May 15, 2010 | Neo |
| 增加解决方案一节，并填充了大量章节。同时对完成这篇文档信心大增 |
| 修订 0.0.4 | 2010 | Neo |
| 这篇文档几乎没有时间和精力编辑，内容增加不多。 |
| 修订 0.0.4 | April 15, 2009 | Neo |
| 这篇文档几乎搁浅，没有时间和精力，没有编辑加入。今天做了一下布局调整，增加一些内容。 |
| 修订 0.0.3 | Sep. 17, 2008 | Neo |
| 加入关于存储的内容 |
| 修订 0.0.1 | May 24, 2008 | Neo |
| 李振韬加入编译团队 |
| 修订 0.0.0 | May 22, 2008 | Neo |
| 这是一个值得纪念的日子 |