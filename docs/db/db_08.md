# 部分 IV. Redis

http://redis.io/

## 第 32 章 Redis 安装

## CentOS 7

[Netkiller OSCM](https://github.com/oscm/shell) 一键安装

```
				curl -s https://raw.githubusercontent.com/oscm/shell/master/database/redis/redis.sh | bash

```

## CentOS 6

安装 fedora 的 YUM 源，

```
				rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-7.noarch.rpm

```

安装 redis

```
				# yum install redis

				# chkconfig redis on

				# service redis start

```

备份配置文件，

```
				# cp /etc/redis.conf /etc/redis.conf.original

```

### 主从同步

主从同步配置非常简单，只需在从服务器 /etc/redis.conf 文件中开启 slaveof 即可

```
					slaveof 192.168.2.1 6379

```

查看 /var/log/redis/redis.log 日志，可以看到同步情况

```

[20274] 09 Jul 13:13:53 * Server started, Redis version 2.4.10
[20274] 09 Jul 13:13:53 * DB loaded from disk: 0 seconds
[20274] 09 Jul 13:13:53 * The server is now ready to accept connections on port 6379
[20274] 09 Jul 13:13:54 * Connecting to MASTER...
[20274] 09 Jul 13:13:54 * MASTER <-> SLAVE sync started
[20274] 09 Jul 13:13:54 * Non blocking connect for SYNC fired the event.
[20274] 09 Jul 13:13:54 * MASTER <-> SLAVE sync: receiving 672 bytes from master
[20274] 09 Jul 13:13:54 * MASTER <-> SLAVE sync: Loading DB in memory
[20274] 09 Jul 13:13:54 * MASTER <-> SLAVE sync: Finished with success

```

### Sentinel

## Ubuntu

```
				$ sudo apt-get install redis-server

```

```

$ dpkg -s redis-server
Package: redis-server
Status: install ok installed
Priority: optional
Section: database
Installed-Size: 208
Maintainer: Chris Lamb <lamby@debian.org>
Architecture: amd64
Source: redis
Version: 2:1.2.6-1
Depends: libc6 (>= 2.7), adduser
Conffiles:
 /etc/redis/redis.conf a19bad63017ec19def2c3a8a07bdc362
 /etc/logrotate.d/redis-server 06755b99ef70d62a56cff94cbfc36de7
 /etc/init.d/redis-server 3742555c10ab16fdd67fcbaf92faf694
 /etc/bash_completion.d/redis-cli 848565df7f222dc03c8d5cb34b9e0188
Description: Persistent key-value database with network interface
 Redis is a key-value database in a similar vein to memcache but the dataset
 is non-volatile. Redis additionally provides native support for atomically
 manipulating and querying data structures such as lists and sets.
 .
 The dataset is stored entirely in memory and periodically flushed to disk.
Homepage: http://code.google.com/p/redis/

```

```

$ cat /etc/redis/redis.conf

# Redis configuration file example

# By default Redis does not run as a daemon. Use 'yes' if you need it.
# Note that Redis will write a pid file in /var/run/redis.pid when daemonized.
daemonize yes

# When run as a daemon, Redis write a pid file in /var/run/redis.pid by default.
# You can specify a custom pid file location here.
pidfile /var/run/redis.pid

# Accept connections on the specified port, default is 6379
port 6379

# If you want you can bind a single interface, if the bind option is not
# specified all the interfaces will listen for connections.
#
bind 127.0.0.1

# Close the connection after a client is idle for N seconds (0 to disable)
timeout 300

# Set server verbosity to 'debug'
# it can be one of:
# debug (a lot of information, useful for development/testing)
# notice (moderately verbose, what you want in production probably)
# warning (only very important / critical messages are logged)
loglevel notice

# Specify the log file name. Also 'stdout' can be used to force
# the demon to log on the standard output. Note that if you use standard
# output for logging but daemonize, logs will be sent to /dev/null
logfile /var/log/redis/redis-server.log

# Set the number of databases. The default database is DB 0, you can select
# a different one on a per-connection basis using SELECT <dbid> where
# dbid is a number between 0 and 'databases'-1
databases 16

################################ SNAPSHOTTING  #################################
#
# Save the DB on disk:
#
#   save <seconds> <changes>
#
#   Will save the DB if both the given number of seconds and the given
#   number of write operations against the DB occurred.
#
#   In the example below the behaviour will be to save:
#   after 900 sec (15 min) if at least 1 key changed
#   after 300 sec (5 min) if at least 10 keys changed
#   after 60 sec if at least 10000 keys changed
save 900 1
save 300 10
save 60 10000

# Compress string objects using LZF when dump .rdb databases?
# For default that's set to 'yes' as it's almost always a win.
# If you want to save some CPU in the saving child set it to 'no' but
# the dataset will likely be bigger if you have compressible values or keys.
rdbcompression yes

# The filename where to dump the DB
dbfilename dump.rdb

# For default save/load DB in/from the working directory
# Note that you must specify a directory not a file name.
dir /var/lib/redis

################################# REPLICATION #################################

# Master-Slave replication. Use slaveof to make a Redis instance a copy of
# another Redis server. Note that the configuration is local to the slave
# so for example it is possible to configure the slave to save the DB with a
# different interval, or to listen to another port, and so on.
#
# slaveof <masterip> <masterport>

# If the master is password protected (using the "requirepass" configuration
# directive below) it is possible to tell the slave to authenticate before
# starting the replication synchronization process, otherwise the master will
# refuse the slave request.
#
# masterauth <master-password>

################################## SECURITY ###################################

# Require clients to issue AUTH <PASSWORD> before processing any other
# commands.  This might be useful in environments in which you do not trust
# others with access to the host running redis-server.
#
# This should stay commented out for backward compatibility and because most
# people do not need auth (e.g. they run their own servers).
#
# requirepass foobared

################################### LIMITS ####################################

# Set the max number of connected clients at the same time. By default there
# is no limit, and it's up to the number of file descriptors the Redis process
# is able to open. The special value '0' means no limts.
# Once the limit is reached Redis will close all the new connections sending
# an error 'max number of clients reached'.
#
# maxclients 128

# Don't use more memory than the specified amount of bytes.
# When the memory limit is reached Redis will try to remove keys with an
# EXPIRE set. It will try to start freeing keys that are going to expire
# in little time and preserve keys with a longer time to live.
# Redis will also try to remove objects from free lists if possible.
#
# If all this fails, Redis will start to reply with errors to commands
# that will use more memory, like SET, LPUSH, and so on, and will continue
# to reply to most read-only commands like GET.
#
# WARNING: maxmemory can be a good idea mainly if you want to use Redis as a
# 'state' server or cache, not as a real DB. When Redis is used as a real
# database the memory usage will grow over the weeks, it will be obvious if
# it is going to use too much memory in the long run, and you'll have the time
# to upgrade. With maxmemory after the limit is reached you'll start to get
# errors for write operations, and this may even lead to DB inconsistency.
#
# maxmemory <bytes>

############################## APPEND ONLY MODE ###############################

# By default Redis asynchronously dumps the dataset on disk. If you can live
# with the idea that the latest records will be lost if something like a crash
# happens this is the preferred way to run Redis. If instead you care a lot
# about your data and don't want to that a single record can get lost you should
# enable the append only mode: when this mode is enabled Redis will append
# every write operation received in the file appendonly.log. This file will
# be read on startup in order to rebuild the full dataset in memory.
#
# Note that you can have both the async dumps and the append only file if you
# like (you have to comment the "save" statements above to disable the dumps).
# Still if append only mode is enabled Redis will load the data from the
# log file at startup ignoring the dump.rdb file.
#
# The name of the append only file is "appendonly.log"
#
# IMPORTANT: Check the BGREWRITEAOF to check how to rewrite the append
# log file in background when it gets too big.

appendonly no

# The fsync() call tells the Operating System to actually write data on disk
# instead to wait for more data in the output buffer. Some OS will really flush
# data on disk, some other OS will just try to do it ASAP.
#
# Redis supports three different modes:
#
# no: don't fsync, just let the OS flush the data when it wants. Faster.
# always: fsync after every write to the append only log . Slow, Safest.
# everysec: fsync only if one second passed since the last fsync. Compromise.
#
# The default is "always" that's the safer of the options. It's up to you to
# understand if you can relax this to "everysec" that will fsync every second
# or to "no" that will let the operating system flush the output buffer when
# it want, for better performances (but if you can live with the idea of
# some data loss consider the default persistence mode that's snapshotting).

appendfsync always
# appendfsync everysec
# appendfsync no

############################### ADVANCED CONFIG ###############################

# Glue small output buffers together in order to send small replies in a
# single TCP packet. Uses a bit more CPU but most of the times it is a win
# in terms of number of queries per second. Use 'yes' if unsure.
glueoutputbuf yes

# Use object sharing. Can save a lot of memory if you have many common
# string in your dataset, but performs lookups against the shared objects
# pool so it uses more CPU and can be a bit slower. Usually it's a good
# idea.
#
# When object sharing is enabled (shareobjects yes) you can use
# shareobjectspoolsize to control the size of the pool used in order to try
# object sharing. A bigger pool size will lead to better sharing capabilities.
# In general you want this value to be at least the double of the number of
# very common strings you have in your dataset.
#
# WARNING: object sharing is experimental, don't enable this feature
# in production before of Redis 1.0-stable. Still please try this feature in
# your development environment so that we can test it better.
shareobjects no
shareobjectspoolsize 1024

```

```
				$ sudo /etc/init.d/redis-server start

```

## 源码编译安装

这里仍然使用 [Netkiller OSCM](https://github.com/oscm/shell) 安装，源码安装

```
				curl -s https://raw.githubusercontent.com/oscm/shell/master/database/redis/source/redis-4.0.1.sh | bash

```

## Test Redis

[`redis.io/commands`](http://redis.io/commands)

```
				$ redis-cli info
				redis_version:1.2.6
				arch_bits:64
				multiplexing_api:epoll
				uptime_in_seconds:859
				uptime_in_days:0
				connected_clients:1
				connected_slaves:0
				used_memory:619490
				used_memory_human:604.97K
				changes_since_last_save:0
				bgsave_in_progress:0
				last_save_time:1311100746
				bgrewriteaof_in_progress:0
				total_connections_received:4
				total_commands_processed:0
				role:master

				$ redis-cli set name neo
				OK
				$ redis-cli get name
				neo

				$ telnet localhost 6379
				Trying ::1...
				telnet: connect to address ::1: Connection refused
				Trying 127.0.0.1...
				Connected to localhost (127.0.0.1).
				Escape character is '^]'.
				get name
				$3
				neo
				quit
				Connection closed by foreign host.

```

## 第 33 章 /etc/redis.conf

参数说明, redis.conf 配置项说明如下：

```

1\. Redis 默认不是以守护进程的方式运行，可以通过该配置项修改，使用 yes 启用守护进程
    daemonize no
2\. 当 Redis 以守护进程方式运行时，Redis 默认会把 pid 写入/var/run/redis.pid 文件，可以通过 pidfile 指定
    pidfile /var/run/redis.pid
3\. 指定 Redis 监听端口，默认端口为 6379，作者在自己的一篇博文中解释了为什么选用 6379 作为默认端口，因为 6379 在手机按键上 MERZ 对应的号码，而 MERZ 取自意大利歌女 Alessia Merz 的名字
    port 6379
4\. 绑定的主机地址
    bind 127.0.0.1
5.当 客户端闲置多长时间后关闭连接，如果指定为 0，表示关闭该功能
    timeout 300
6\. 指定日志记录级别，Redis 总共支持四个级别：debug、verbose、notice、warning，默认为 verbose
    loglevel verbose
7\. 日志记录方式，默认为标准输出，如果配置 Redis 为守护进程方式运行，而这里又配置为日志记录方式为标准输出，则日志将会发送给/dev/null
    logfile stdout
8\. 设置数据库的数量，默认数据库为 0，可以使用 SELECT <dbid>命令在连接上指定数据库 id
    databases 16
9\. 指定在多长时间内，有多少次更新操作，就将数据同步到数据文件，可以多个条件配合
    save <seconds> <changes>
    Redis 默认配置文件中提供了三个条件：
    save 900 1
    save 300 10
    save 60 10000
    分别表示 900 秒（15 分钟）内有 1 个更改，300 秒（5 分钟）内有 10 个更改以及 60 秒内有 10000 个更改。

10\. 指定存储至本地数据库时是否压缩数据，默认为 yes，Redis 采用 LZF 压缩，如果为了节省 CPU 时间，可以关闭该选项，但会导致数据库文件变的巨大
    rdbcompression yes
11\. 指定本地数据库文件名，默认值为 dump.rdb
    dbfilename dump.rdb
12\. 指定本地数据库存放目录
    dir ./
13\. 设置当本机为 slav 服务时，设置 master 服务的 IP 地址及端口，在 Redis 启动时，它会自动从 master 进行数据同步
    slaveof <masterip> <masterport>
14\. 当 master 服务设置了密码保护时，slav 服务连接 master 的密码
    masterauth <master-password>
15\. 设置 Redis 连接密码，如果配置了连接密码，客户端在连接 Redis 时需要通过 AUTH <password>命令提供密码，默认关闭
    requirepass foobared
16\. 设置同一时间最大客户端连接数，默认无限制，Redis 可以同时打开的客户端连接数为 Redis 进程可以打开的最大文件描述符数，如果设置 maxclients 0，表示不作限制。当客户端连接数到达限制时，Redis 会关闭新的连接并向客户端返回 max number of clients reached 错误信息
    maxclients 128
17\. 指定 Redis 最大内存限制，Redis 在启动时会把数据加载到内存中，达到最大内存后，Redis 会先尝试清除已到期或即将到期的 Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。Redis 新的 vm 机制，会把 Key 存放内存，Value 会存放在 swap 区
    maxmemory <bytes>
18\. 指定是否在每次更新操作后进行日志记录，Redis 在默认情况下是异步的把数据写入磁盘，如果不开启，可能会在断电时导致一段时间内的数据丢失。因为 redis 本身同步数据文件是按上面 save 条件来同步的，所以有的数据会在一段时间内只存在于内存中。默认为 no
    appendonly no
19\. 指定更新日志文件名，默认为 appendonly.aof
     appendfilename appendonly.aof
20\. 指定更新日志条件，共有 3 个可选值： 
    no：表示等操作系统进行数据缓存同步到磁盘（快） 
    always：表示每次更新操作后手动调用 fsync()将数据写到磁盘（慢，安全） 
    everysec：表示每秒同步一次（折衷，默认值）
    appendfsync everysec

21\. 指定是否启用虚拟内存机制，默认值为 no，简单的介绍一下，VM 机制将数据分页存放，由 Redis 将访问量较少的页即冷数据 swap 到磁盘上，访问多的页面由磁盘自动换出到内存中（在后面的文章我会仔细分析 Redis 的 VM 机制）
     vm-enabled no
22\. 虚拟内存文件路径，默认值为/tmp/redis.swap，不可多个 Redis 实例共享
     vm-swap-file /tmp/redis.swap
23\. 将所有大于 vm-max-memory 的数据存入虚拟内存,无论 vm-max-memory 设置多小,所有索引数据都是内存存储的(Redis 的索引数据 就是 keys),也就是说,当 vm-max-memory 设置为 0 的时候,其实是所有 value 都存在于磁盘。默认值为 0
     vm-max-memory 0
24\. Redis swap 文件分成了很多的 page，一个对象可以保存在多个 page 上面，但一个 page 上不能被多个对象共享，vm-page-size 是要根据存储的 数据大小来设定的，作者建议如果存储很多小对象，page 大小最好设置为 32 或者 64bytes；如果存储很大大对象，则可以使用更大的 page，如果不 确定，就使用默认值
     vm-page-size 32
25\. 设置 swap 文件中的 page 数量，由于页表（一种表示页面空闲或使用的 bitmap）是在放在内存中的，，在磁盘上每 8 个 pages 将消耗 1byte 的内存。
     vm-pages 134217728
26\. 设置访问 swap 文件的线程数,最好不要超过机器的核数,如果设置为 0,那么所有对 swap 文件的操作都是串行的，可能会造成比较长时间的延迟。默认值为 4
     vm-max-threads 4
27\. 设置在向客户端应答时，是否把较小的包合并为一个包发送，默认为开启
    glueoutputbuf yes
28\. 指定在超过一定的数量或者最大的元素超过某一临界值时，采用一种特殊的哈希算法
    hash-max-zipmap-entries 64
    hash-max-zipmap-value 512
29\. 指定是否激活重置哈希，默认为开启（后面在介绍 Redis 的哈希算法时具体介绍）
    activerehashing yes
30\. 指定包含其它的配置文件，可以在同一主机上多个 Redis 实例之间使用同一份配置文件，而同时各个实例又拥有自己的特定配置文件
    include /path/to/local.conf

```

## 密码认证

打开 /etc/redis.conf 修改 requirepass 配置项

```
				# vim /etc/redis.conf

				requirepass test123

```

测试

```

# service redis restart
Stopping redis-server:                                     [  OK  ]
Starting redis-server:                                     [  OK  ]

# redis-cli
redis 127.0.0.1:6379> set h helloworld
(error) ERR operation not permitted

```

auth test123

```

redis 127.0.0.1:6379> auth test123
OK
redis 127.0.0.1:6379> set h helloworld
OK
redis 127.0.0.1:6379> get h
"helloworld"
redis 127.0.0.1:6379> exit

```

redis-cli -a 参数指定密码

```

# redis-cli -a test123
redis 127.0.0.1:6379> set h helloworld
OK
redis 127.0.0.1:6379> get h
"helloworld"
redis 127.0.0.1:6379> exit

```

通过 config 动态改变密码，无需重新启动 redis 进程

```

# redis-cli -a test123
redis 127.0.0.1:6379> config get requirepass
1) "requirepass"
2) "test123"
redis 127.0.0.1:6379> config set requirepass passabc
OK
redis 127.0.0.1:6379> auth passabc
OK
redis 127.0.0.1:6379> set h helloworld
OK
redis 127.0.0.1:6379> get h
"helloworld"
redis 127.0.0.1:6379> exit

```

注意：config 不能保存到配置文件

```

# grep requirepass /etc/redis.conf

# If the master is password protected (using the "requirepass" configuration
# requirepass foobared
requirepass test123

```

master/slave 模式， master 有密码, slave 怎样配置？

```
				masterauth password

```

## maxmemory-policy TTL 过期策略配置

```
				1、volatile-lru：只对设置了过期时间的 key 进行 LRU（默认值）
				2、allkeys-lru ： 删除 lru 算法的 key
				3、volatile-random：随机删除即将过期 key
				4、allkeys-random：随机删除
				5、volatile-ttl ： 删除即将过期的
				6、noeviction ： 永不过期，返回错误

```

## 第 34 章 redis-cli - Command-line client to redis-server

## 命令参数

### password

```

-a <password>      Password to use when connecting to the server.
[root@netkiller conf.d]# redis-cli -a hsM8NK8b71vFQKFOS55jbWJrA1TYgI4e

```

### raw

--raw Use raw formatting for replies (default when STDOUT is not a tty).

有时显示这样的数据

```

[hadoop@netkiller ~]$ redis-cli 
127.0.0.1:6379> ZREVRANGE FASTNEWS_DATA_STORE_KEY 0 1
1) "{\"title\":\"\xe4\xb8\x8a\xe8\xaf\x81\xe7\xbb\xbc\xe6\x8c\x87\xe5\x91\xa8\xe7\xba\xbf\xe4\xba\x94\xe8\xbf\x9e\xe9\x98\xb3\xef\xbc\x8c\xe5\x88\x9b\xe4\xb8\x9a\xe6\x9d\xbf\xe8\xbf\x9e\xe7\xbb\xad\xe4\xb8\xa4\xe5\x91\xa8\xe5\xa4\xa7\xe8\xb7\x8c\xef\xbc\x9b    \xe4\xb8\x8a\xe8\xaf\x81\xe7\xbb\xbc\xe6\x8c\x87\xe5\x91\xa8\xe4\xba\x94\xe6\x94\xb6\xe7\x9b\x98\xe8\xb7\x8c0.23%\xef\xbc\x8c\xe4\xb8\x8b\xe8\xb7\x8c7.60\xe7\x82\xb9\xef\xbc\x8c\xe6\x8a\xa53237.98\xe7\x82\xb9\xef\xbc\x9b    \xe6\xb7\xb1\xe8\xaf\x81\xe6\x88\x90\xe6\x8c\x87\xe5\x91\xa8\xe4\xba\x94\xe6\x94\xb6\xe7\x9b\x98\xe8\xb7\x8c0.03%\xef\xbc\x8c\xe4\xb8\x8b\xe8\xb7\x8c3.50\xe7\x82\xb9\xef\xbc\x8c\xe6\x8a\xa510363.48\xe7\x82\xb9\xef\xbc\x9b    \xe6\xb2\xaa\xe6\xb7\xb1300\xe8\x82\xa1\xe6\x8c\x87\xe5\x91\xa8\xe4\xba\x94\xe6\x94\xb6\xe7\x9b\x98\xe8\xb7\x8c0.52%\xef\xbc\x8c\xe4\xb8\x8b\xe8\xb7\x8c19.49\xe7\x82\xb9\xef\xbc\x8c\xe6\x8a\xa53728.39\xe7\x82\xb9\xef\xbc\x9b    \xe5\x88\x9b\xe4\xb8\x9a\xe6\x9d\xbf\xe6\x8c\x87\xe6\x95\xb0\xe5\x91\xa8\xe4\xba\x94\xe6\x94\xb6\xe7\x9b\x98\xe6\xb6\xa80.08%\xef\xbc\x8c\xe4\xb8\x8a\xe6\xb6\xa81.31\xe7\x82\xb9\xef\xbc\x8c\xe6\x8a\xa51689.92\xe7\x82\xb9\\r\\n\"}"
2) "{\"title\":\"\xe4\xb8\x8a\xe8\xaf\x81\xe7\xbb\xbc\xe6\x8c\x87\xe5\x91\xa8\xe4\xba\x94\xe6\x94\xb6\xe7\x9b\x98\xe4\xb8\x8b\xe8\xb7\x8c0.21%\xef\xbc\x8c\xe6\x8a\xa53237.98\xe7\x82\xb9\xef\xbc\x9b\xe6\xb7\xb1\xe8\xaf\x81\xe6\x88\x90\xe6\x8c\x87\xe5\x91\xa8\xe4\xba\x94\xe6\x94\xb6\xe7\x9b\x98\xe5\xbe\xae\xe8\xb7\x8c0.02%\xef\xbc\x8c\xe6\x8a\xa510364.82\xe7\x82\xb9\xef\xbc\x9b\xe5\x88\x9b\xe4\xb8\x9a\xe6\x9d\xbf\xe6\x8c\x87\xe6\x95\xb0\xe5\x91\xa8\xe4\xba\x94\xe6\x94\xb6\xe7\x9b\x98\xe5\xbe\xae\xe6\xb6\xa80.09%\xef\xbc\x8c\xe6\x8a\xa51690.15\xe7\x82\xb9\"}"
127.0.0.1:6379> 

```

加入 --raw 参数后可以显示可以阅读的数据

```

[hadoop@VM_3_2_centos ~]$ redis-cli --raw
127.0.0.1:6379> ZREVRANGE FASTNEWS_DATA_STORE_KEY 0 1
{"title":"上证综指周线五连阳，创业板连续两周大跌"}
{"title":"上证综指周五收盘下跌 0.21%，报 3237.98 点"}
127.0.0.1:6379> 

```

## --latency Enter a special mode continuously sampling latency.

参数的功能是从客户端发出一条命令到客户端接受到该命令的反馈所用的最长响应时间

```
				# redis-cli --latency -h 192.168.2.1
				min: 1, max: 210, avg: 3.64 (13453 samples)

```

## auth

认证密码

```
				[root@netkiller ~]# redis-cli
				127.0.0.1:6379> keys *
				(error) NOAUTH Authentication required.
				127.0.0.1:6379> auth hsM8NKb71vjbWJrA1TYgI4
				OK
				127.0.0.1:6379> keys *
				1) "HK50(1605)"
				2) "GBPUSD"
				3) "USDCHF"
				4) "SP500(1609)"
				5) "NZDJPY"
				6) "AUDNZD"
				7) "EURGBP"
				8) "CLN6"
				9) "BU6"

```

## MONITOR

```
				$ redis-cli monitor

```

## info

```

redis 127.0.0.1:6379> info
redis_version:2.4.10
redis_git_sha1:00000000
redis_git_dirty:0
arch_bits:64
multiplexing_api:epoll
gcc_version:4.4.6
process_id:29663
uptime_in_seconds:1189
uptime_in_days:0
lru_clock:1018411
used_cpu_sys:0.10
used_cpu_user:0.09
used_cpu_sys_children:0.01
used_cpu_user_children:0.00
connected_clients:1
connected_slaves:0
client_longest_output_list:0
client_biggest_input_buf:0
blocked_clients:0
used_memory:730664
used_memory_human:713.54K
used_memory_rss:7225344
used_memory_peak:730720
used_memory_peak_human:713.59K
mem_fragmentation_ratio:9.89
mem_allocator:jemalloc-2.2.5
loading:0
aof_enabled:0
changes_since_last_save:0
bgsave_in_progress:0
last_save_time:1373332622
bgrewriteaof_in_progress:0
total_connections_received:4
total_commands_processed:14
expired_keys:0
evicted_keys:0
keyspace_hits:3
keyspace_misses:0
pubsub_channels:0
pubsub_patterns:0
latest_fork_usec:744
vm_enabled:0
role:master
db0:keys=4,expires=0
redis 127.0.0.1:6379>

```

## save/bgsave/lastsave

save/bgsave 保存持久化将数据，lastsave 查看相关信息

```

redis 127.0.0.1:6379> save
OK
redis 127.0.0.1:6379> bgsave
Background saving started
redis 127.0.0.1:6379> lastsave
(integer) 1373335757

```

## config

```

CONFIG GET dir
CONFIG GET dbfilename

config set stop-writes-on-bgsave-error yes
CONFIG SET dir /tmp
CONFIG SET dbfilename temp.rdb

```

## keys

查询所有 key

```

172.18.52.15:6379> keys *
 1) "www.example.com:743f10d0f1dc569ed5893856e14c1fb7captcha"
 2) "www.example.com:d88e0b6c54a235763dd731bcc0914439captcha"
 3) "www.example.com:17f9091cb44f3cc5bb411eb801f07be8member_login"
 4) "www.example.com:10ff594fd42f4c81212020555cfb586amember_login_input"
 5) "www.example.com:a759ba5232ce324d0e6ae8da9290beaecaptcha"
 6) "www.example.com:37c78410af02d66a542d15b9707f215bcaptcha"
 7) "www.example.com:9f5070e217f4eac9a1d15f9b8dbe7148deposit_1_temp_var"
 8) "www.example.com:6c1a13c9396df2c35613043923bfe338captcha"
 9) "www.example.com:b611080c0627154871ea0e1498793238captcha"
10) "www.example.com:2792241f8d0f075528db2b50e0c9c684member_login"

```

查询指定 key

```

172.18.50.15:6379> set name neo
OK
172.18.50.15:6379> keys name
1) "name"			

```

## 字符串操作

### set/get/del

```

172.18.52.15:6379> set name neo
OK
172.18.52.15:6379> get name 
"neo"
172.18.52.15:6379> keys name
1) "name"
172.18.52.15:6379> del name
(integer) 1
172.18.52.15:6379> get name
(nil)			

```

### setnx

**SETNX key value**

当 key 不存在时将 key 的值设为 value，若给定的 key 已经存在，则 SETNX 不做任何动作。SETNX 是(SET if Not eXists) (如果不存在，则 SET)的简写。

```

返回值：
	设置成功，返回 1
	设置失败，返回 0

redis> EXISTS neo             # neo 不存在
(integer) 0

redis> SETNX neo "chen"    	  # neo 设置成功
(integer) 1

redis> SETNX neo "netkiller"  # 尝试覆盖 neo ，失败
(integer) 0

redis> GET neo                # 没有被覆盖
"chen"	

```

## expire/ttl

EXPIRE 设置过期时间, TTL 可以查询过期时间倒计时。

```

172.18.52.165:6379> set name neo
OK
172.18.52.165:6379> ttl name
(integer) -1
172.18.52.165:6379> expire name 30
(integer) 1
172.18.52.165:6379> ttl name
(integer) 22
172.18.52.165:6379> ttl name
(integer) 9
172.18.52.165:6379> ttl name
(integer) -1
172.18.52.165:6379> get  name
(nil)

```

注意 ttl 返回-1 有两种情况，一是没有设置过期时间，另一种是该 key 已经过期不存在。

## 获取 key 类型

```

root@netkiller ~ % redis-cli 
127.0.0.1:6379> TYPE "logstash:redis"
list		

```

## LIST 数据类型

获取 list 列表长度

```

127.0.0.1:6379> TYPE "logstash:redis"
list
127.0.0.1:6379> LLEN "logstash:redis"
(integer) 69
127.0.0.1:6379> lpop "logstash:redis"
127.0.0.1:6379> 	
127.0.0.1:6379> LLEN "logstash:redis"
(integer) 68

```

```

1\. LPUSH/LPUSHX/LRANGE:
/> redis-cli    #在 Shell 提示符下启动 redis 客户端工具。
redis 127.0.0.1:6379> del queue:test
(integer) 1
#queue:test 键并不存在，该命令会创建该键及与其关联的 List，之后在将参数中的 values 从左到右依次插入。
redis 127.0.0.1:6379> lpush queue:test a b c d
(integer) 4
#取从位置 0 开始到位置 2 结束的 3 个元素。
redis 127.0.0.1:6379> lrange queue:test 0 2
1) "d"
2) "c"
3) "b"
#取链表中的全部元素，其中 0 表示第一个元素，-1 表示最后一个元素。
redis 127.0.0.1:6379> lrange queue:test 0 -1
1) "d"
2) "c"
3) "b"
4) "a"
#queue:test2 键此时并不存在，因此该命令将不会进行任何操作，其返回值为 0。
redis 127.0.0.1:6379> lpushx queue:test2 e
(integer) 0
#可以看到 queue:test2 没有关联任何 List Value。
redis 127.0.0.1:6379> lrange queue:test2 0 -1
(empty list or set)
#queue:test 键此时已经存在，所以该命令插入成功，并返回链表中当前元素的数量。
redis 127.0.0.1:6379> lpushx queue:test e
(integer) 5
#获取该键的 List Value 的头部元素。
redis 127.0.0.1:6379> lrange queue:test 0 0
1) "e"

2\. LPOP/LLEN:
redis 127.0.0.1:6379> lpush queue:test a b c d
(integer) 4
redis 127.0.0.1:6379> lpop queue:test
"d"
redis 127.0.0.1:6379> lpop queue:test
"c"
#在执行 lpop 命令两次后，链表头部的两个元素已经被弹出，此时链表中元素的数量是 2
redis 127.0.0.1:6379> llen queue:test
(integer) 2

3\. LREM/LSET/LINDEX/LTRIM:
#为后面的示例准备测试数据。
redis 127.0.0.1:6379> lpush queue:test a b c d a c
(integer) 6
#从头部(left)向尾部(right)变量链表，删除 2 个值等于 a 的元素，返回值为实际删除的数量。
redis 127.0.0.1:6379> lrem queue:test 2 a
(integer) 2
#看出删除后链表中的全部元素。
redis 127.0.0.1:6379> lrange queue:test 0 -1
1) "c"
2) "d"
3) "c"
4) "b"
#获取索引值为 1(头部的第二个元素)的元素值。
redis 127.0.0.1:6379> lindex queue:test 1
"d"
#将索引值为 1(头部的第二个元素)的元素值设置为新值 e。
redis 127.0.0.1:6379> lset queue:test 1 e
OK
#查看是否设置成功。
redis 127.0.0.1:6379> lindex queue:test 1
"e"
#索引值 6 超过了链表中元素的数量，该命令返回 nil。
redis 127.0.0.1:6379> lindex queue:test 6
(nil)
#设置的索引值 6 超过了链表中元素的数量，设置失败，该命令返回错误信息。
redis 127.0.0.1:6379> lset queue:test 6 hh
(error) ERR index out of range
#仅保留索引值 0 到 2 之间的 3 个元素，注意第 0 个和第 2 个元素均被保留。
redis 127.0.0.1:6379> ltrim queue:test 0 2
OK
#查看 trim 后的结果。
redis 127.0.0.1:6379> lrange queue:test 0 -1
1) "c"
2) "e"
3) "c"

4\. LINSERT:
#删除该键便于后面的测试。
redis 127.0.0.1:6379> del queue:test
(integer) 1
#为后面的示例准备测试数据。
redis 127.0.0.1:6379> lpush queue:test a b c d e
(integer) 5
#在 a 的前面插入新元素 a1。
redis 127.0.0.1:6379> linsert queue:test before a a1
(integer) 6
#查看是否插入成功，从结果看已经插入。注意 lindex 的 index 值是 0-based。
redis 127.0.0.1:6379> lindex queue:test 0
"e"
#在 e 的后面插入新元素 e2，从返回结果看已经插入成功。
redis 127.0.0.1:6379> linsert queue:test after e e2
(integer) 7
#再次查看是否插入成功。
redis 127.0.0.1:6379> lindex queue:test 1
"e2"
#在不存在的元素之前或之后插入新元素，该命令操作失败，并返回-1。
redis 127.0.0.1:6379> linsert queue:test after k a
(integer) -1
#为不存在的 Key 插入新元素，该命令操作失败，返回 0。
redis 127.0.0.1:6379> linsert queue:test1 after a a2
(integer) 0

5\. RPUSH/RPUSHX/RPOP/RPOPLPUSH:
#删除该键，以便于后面的测试。
redis 127.0.0.1:6379> del queue:test
(integer) 1
#从链表的尾部插入参数中给出的 values，插入顺序是从左到右依次插入。
redis 127.0.0.1:6379> rpush queue:test a b c d
(integer) 4
#通过 lrange 的可以获悉 rpush 在插入多值时的插入顺序。
redis 127.0.0.1:6379> lrange queue:test 0 -1
1) "a"
2) "b"
3) "c"
4) "d"
#该键已经存在并且包含 4 个元素，rpushx 命令将执行成功，并将元素 e 插入到链表的尾部。
redis 127.0.0.1:6379> rpushx queue:test e
(integer) 5
#通过 lindex 命令可以看出之前的 rpushx 命令确实执行成功，因为索引值为 4 的元素已经是新元素了。
redis 127.0.0.1:6379> lindex queue:test 4
"e"
#由于 queue:test2 键并不存在，因此该命令不会插入数据，其返回值为 0。
redis 127.0.0.1:6379> rpushx queue:test2 e
(integer) 0
#在执行 rpoplpush 命令前，先看一下 queue:test 中链表的元素有哪些，注意他们的位置关系。
redis 127.0.0.1:6379> lrange queue:test 0 -1
1) "a"
2) "b"
3) "c"
4) "d"
5) "e"
#将 queue:test 的尾部元素 e 弹出，同时再插入到 queue:test2 的头部(原子性的完成这两步操作)。
redis 127.0.0.1:6379> rpoplpush queue:test queue:test2
"e"
#通过 lrange 命令查看 queue:test 在弹出尾部元素后的结果。
redis 127.0.0.1:6379> lrange queue:test 0 -1
1) "a"
2) "b"
3) "c"
4) "d"
#通过 lrange 命令查看 queue:test2 在插入元素后的结果。
redis 127.0.0.1:6379> lrange queue:test2 0 -1
1) "e"
#将 source 和 destination 设为同一键，将 queue:test 中的尾部元素移到其头部。
redis 127.0.0.1:6379> rpoplpush queue:test queue:test
"d"
#查看移动结果。
redis 127.0.0.1:6379> lrange queue:test 0 -1
1) "d"
2) "a"
3) "b"
4) "c"			

```

## set 无序字符集合

Set 和 List 类型不同的是，Set 集合中不允许出现重复的元素，和 List 类型相比，Set 类型在功能上还存在着一个非常重要的特性，即在服务器端完成多个 Sets 之间的聚合计算操作，如 unions、intersections 和 differences。由于这些操作均在服务端完成，因此效率极高，而且也节省了大量的网络 IO 开销。

1\. SADD/SMEMBERS/SCARD/SISMEMBER:

```

    #插入测试数据，由于该键 set:test 之前并不存在，因此参数中的三个成员都被正常插入。
    redis 127.0.0.1:6379> sadd set:test a b c
    (integer) 3
    #由于参数中的 a 在 set:test 中已经存在，因此本次操作仅仅插入了 d 和 e 两个新成员。
    redis 127.0.0.1:6379> sadd set:test a d e
    (integer) 2
    #判断 a 是否已经存在，返回值为 1 表示存在。
    redis 127.0.0.1:6379> sismember set:test a
    (integer) 1
    #判断 f 是否已经存在，返回值为 0 表示不存在。
    redis 127.0.0.1:6379> sismember set:test f
    (integer) 0
    #通过 smembers 命令查看插入的结果，从结果可以，输出的顺序和插入顺序无关。
    redis 127.0.0.1:6379> smembers set:test
    1) "c"
    2) "d"
    3) "a"
    4) "b"
    5) "e"
    #获取 Set 集合中元素的数量。
    redis 127.0.0.1:6379> scard set:test
    (integer) 5

```

2\. SPOP/SREM/SRANDMEMBER/SMOVE:

```

    #删除该键，便于后面的测试。
    redis 127.0.0.1:6379> del set:test
    (integer) 1
    #为后面的示例准备测试数据。
    redis 127.0.0.1:6379> sadd set:test a b c d
    (integer) 4
    #查看 Set 中成员的位置。
    redis 127.0.0.1:6379> smembers set:test
    1) "c"
    2) "d"
    3) "a"
    4) "b"
    #从结果可以看出，该命令确实是随机的返回了某一成员。
    redis 127.0.0.1:6379> srandmember set:test
    "c"
    #Set 中尾部的成员 b 被移出并返回，事实上 b 并不是之前插入的第一个或最后一个成员。
    redis 127.0.0.1:6379> spop set:test
    "b"
    #查看移出后 Set 的成员信息。
    redis 127.0.0.1:6379> smembers set:test
    1) "c"
    2) "d"
    3) "a"
    #从 Set 中移出 a、d 和 f 三个成员，其中 f 并不存在，因此只有 a 和 d 两个成员被移出，返回为 2。
    redis 127.0.0.1:6379> srem set:test a d f
    (integer) 2
    #查看移出后的输出结果。
    redis 127.0.0.1:6379> smembers set:test
    1) "c"
    #为后面的 smove 命令准备数据。
    redis 127.0.0.1:6379> sadd set:test a b
    (integer) 2
    redis 127.0.0.1:6379> sadd set:test2 c d
    (integer) 2
    #将 a 从 set:test 移到 set:test2，从结果可以看出移动成功。
    redis 127.0.0.1:6379> smove set:test set:test2 a
    (integer) 1
    #再次将 a 从 set:test 移到 set:test2，由于此时 a 已经不是 set:test 的成员了，因此移动失败并返回 0。
    redis 127.0.0.1:6379> smove set:test set:test2 a
    (integer) 0
    #分别查看 set:test 和 set:test2 的成员，确认移动是否真的成功。
    redis 127.0.0.1:6379> smembers set:test
    1) "b"
    redis 127.0.0.1:6379> smembers set:test2
    1) "c"
    2) "d"
    3) "a"

```

3\. SDIFF/SDIFFSTORE/SINTER/SINTERSTORE:

```

    #为后面的命令准备测试数据。
    redis 127.0.0.1:6379> sadd set:test a b c d
    (integer) 4
    redis 127.0.0.1:6379> sadd set:test2 c
    (integer) 1
    redis 127.0.0.1:6379> sadd set:test3 a c e
    (integer) 3
    #set:test 和 set:test2 相比，a、b 和 d 三个成员是两者之间的差异成员。再用这个结果继续和 set:test3 进行差异比较，b 和 d 是 set:test3 不存在的成员。
    redis 127.0.0.1:6379> sdiff set:test set:test2 set:test3
    1) "d"
    2) "b"
    #将 3 个集合的差异成员存在在 diffkey 关联的 Set 中，并返回插入的成员数量。
    redis 127.0.0.1:6379> sdiffstore diffkey set:test set:test2 set:test3
    (integer) 2
    #查看一下 sdiffstore 的操作结果。
    redis 127.0.0.1:6379> smembers diffkey
    1) "d"
    2) "b"
    #从之前准备的数据就可以看出，这三个 Set 的成员交集只有 c。
    redis 127.0.0.1:6379> sinter set:test set:test2 set:test3
    1) "c"
    #将 3 个集合中的交集成员存储到与 interkey 关联的 Set 中，并返回交集成员的数量。
    redis 127.0.0.1:6379> sinterstore interkey set:test set:test2 set:test3
    (integer) 1
    #查看一下 sinterstore 的操作结果。
    redis 127.0.0.1:6379> smembers interkey
    1) "c"
    #获取 3 个集合中的成员的并集。    
    redis 127.0.0.1:6379> sunion set:test set:test2 set:test3
    1) "b"
    2) "c"
    3) "d"
    4) "e"
    5) "a"
    #将 3 个集合中成员的并集存储到 unionkey 关联的 set 中，并返回并集成员的数量。
    redis 127.0.0.1:6379> sunionstore unionkey set:test set:test2 set:test3
    (integer) 5
    #查看一下 suiionstore 的操作结果。
    redis 127.0.0.1:6379> smembers unionkey
    1) "b"
    2) "c"
    3) "d"
    4) "e"
    5) "a"

```

## zset (有序集合)

添加到集合

```

root@netkiller ~ % redis-cli -n 16
127.0.0.1:6379[16]> zadd book 1 "Linux"
1
127.0.0.1:6379[16]> zadd book 2 "Java"
1
127.0.0.1:6379[16]> zadd book 3 "Python"
1
127.0.0.1:6379[16]> zadd book 4 "PHP"
1
127.0.0.1:6379[16]> 

```

zrange 查看集合内容

```

127.0.0.1:6379[16]> zrange book 0 -1 withscores
1) "Linux"
2) "1"
3) "Java"
4) "2"
5) "Perl"
6) "5"

```

指定开始和结束范围

```

127.0.0.1:6379[16]> zrange book 0 4
Linux
Java
Python
PHP

127.0.0.1:6379[16]> zrange book 1 4
Java
Python
PHP

127.0.0.1:6379[16]> zrange book 2 3
Python
PHP		

```

zrem 删除集合成员

```

127.0.0.1:6379[16]> zadd book 5 "Rabby"
1
127.0.0.1:6379[16]> zrange book 4 5
Perl
Rabby
127.0.0.1:6379[16]> zrem book Rabby
1
127.0.0.1:6379[16]> zrange book 4 5
Perl

127.0.0.1:6379[16]> zrem book PHP Python
2

```

zcard 返回成员数量

```

127.0.0.1:6379[16]> zcard book
3

```

## Pub/Sub 订阅与发布

redis 提供基本的 MQ 功能，下面我们做一个演示

开启第一个终端窗口，订阅 first second

```
				$ redis-cli
				redis 127.0.0.1:6379> SUBSCRIBE first second
				Reading messages... (press Ctrl-C to quit)
				1) "subscribe"
				2) "first"
				3) (integer) 1
				1) "subscribe"
				2) "second"
				3) (integer) 2

```

开启第二个终端窗口，分别发送 first second

```
				$ redis-cli
				redis 127.0.0.1:6379> PUBLISH second Hello
				(integer) 1
				redis 127.0.0.1:6379> PUBLISH first Helloworld!!!
				(integer) 1
				redis 127.0.0.1:6379> quit

```

现在切换到第一个终端窗口，应该能够看到发送过来的字符串

```
				$ redis-cli
				redis 127.0.0.1:6379> SUBSCRIBE first second
				Reading messages... (press Ctrl-C to quit)
				1) "subscribe"
				2) "first"
				3) (integer) 1
				1) "subscribe"
				2) "second"
				3) (integer) 2

				1) "message"
				2) "second"
				3) "Hello"

				1) "message"
				2) "first"
				3) "Helloworld!!!"

```

## flushdb 清空 Redis 数据

```

root@netkiller ~ % redis-cli 
127.0.0.1:6379> flushdb
OK
127.0.0.1:6379> 			

```

## 第 35 章 redis-benchmark 测试工具

redis-benchmark 基准性能测试

```

用法 redis-benchmark [-h <host>][-p ] [-c <clients>][-n ]> [-k <boolean>]

选项：

选项	说明
-h <hostname>	主机名 （默认 127.0.0.1）
-p <port>	主机端口 （默认 6379）
-s <socket>	UNIX socket （会覆盖 -h -p 设置的内容）
-a <password>	密码（密码错误之类不会直接保错，而是在操作时才会保错，这时可以使用 Redis 的 AUTH 命令再次认证）
-c <clients>	客户端的并发数量（默认是 50）
-n <requests>	客户端请求总量（默认是 100000）
-d <size>		使用 SET/GET 添加的数据的字节大小 (默认 2)
-dbnum <db>		选择一个数据库进行测试 (默认 0)
-k <boolean>	客户端是否使用 keepalive，1 为使用，0 为不使用，（默认为 1）
-r <keyspacelen>使用 SET/GET/INCR 命令添加数据 key, SADD 添加随机数据，keyspacelen 指定的是添加 键的数量
-P <numreq>		每个请求 pipeline 的数据量（默认为 1，没有 pipeline ）
-q				仅仅显示 redis-benchmark 的 requests per second 信息
--csv			将结果按照 csv 格式输出，便于后续处理
-l				循环测试
-t <tests>		可以对指定命令进行基准测试
-I				空闲模式 只打开 N 个空闲连接并等待。

```

代表 256 各个客户端同时请求 Redis，一 共执行 20000 次。redis-benchmark 会对各类数据结构的命令进行测试，并给 出性能指标：

```
			redis-benchmark -c 256 -n 20000

```

## 第 36 章 Redis Cluster

## 第 37 章 Redis 通信协议

## 切换 DB

select n 切换 DB， n 表示数据库 ID

```
				# telnet 192.168.41.160 6379
				Trying 192.168.41.160...
				Connected to 192.168.41.160.
				Escape character is '^]'.
				select 1
				+OK

```

## 监控

telnet 方式

```
				# telnet 172.18.52.13 6379
				Trying 172.18.52.163...
				Connected to 172.18.52.13.
				Escape character is '^]'.
				MONITOR
				+OK
				+1425454378.190210 "MONITOR"
				+1425454381.165317 "GET" "admin:633"
				+1425454381.165725 "SET" "admin:633" "{\"id\":\"633\",\"username\":\"7209\",\"password\":\"eea5981a4fd021b8d78f8431084ba760\",\"status\":\"N\",\"belong_user_id\":\"133\",\"level_id\":\"67\",\"create_time\":1425454381,\"session_id\":\"l1s609t9gq8nj7vc94hb1i3s25\"}"
				+1425454381.166088 "EXPIRE" "admin:633" "3600"
				+1425454387.956387 "GET" "admin:633"

```

使用 nc 监控状态

```
				# (echo -en "MONITOR\r\n"; sleep 10) | nc 172.18.52.13 6379

```

## 第 38 章 phpRedisAdmin

[`github.com/ErikDubbelboer/phpRedisAdmin`](https://github.com/ErikDubbelboer/phpRedisAdmin)

Example

You can find an example database at http://dubbelboer.com/phpRedisAdmin/

第一种方法

```
			git clone https://github.com/ErikDubbelboer/phpRedisAdmin
			cd phpRedisAdmin
			git clone https://github.com/nrk/predis

```

第二种方法

```
			git clone https://github.com/ErikDubbelboer/phpRedisAdmin.git
			cd phpRedisAdmin
			git submodule init
			git submodule update

```

## 第 39 章 Redis 开发

## 消息订阅与发布

订阅

```

<?php
$redis = new Redis();
$redis->connect('127.0.0.1',6379);
$channel = $argv[1];  // channel
$redis->subscribe(array('channel'.$channel), 'callback');
function callback($instance, $channelName, $message) {
  echo $channelName, "==>", $message,PHP_EOL;
}			

```

发布

```

<?php
$redis = new Redis();
$redis->connect('127.0.0.1',6379);
$channel = $argv[1];  // channel
$msg = $argv[2];     // msg
$redis->publish('channel'.$channel, $msg);			

```

## 第 40 章 A fast, light-weight proxy for memcached and redis

https://github.com/twitter/twemproxy

## 第 41 章 FAQ

## 清空数据库

```
				FLUSHDB - Removes data from your connection's CURRENT database.
				FLUSHALL - Removes data from ALL databases.

```

## (error) MISCONF Redis is configured to save RDB snapshots

(error) MISCONF Redis is configured to save RDB snapshots, but it is currently not able to persist on disk. Commands that may modify the data set are disabled, because this instance is configured to report errors during writes if RDB snapshotting fails (stop-writes-on-bgsave-error option). Please check the Redis logs for details about the RDB error.

临时解决方案

```

# redis-cli

127.0.0.1:6379> config set stop-writes-on-bgsave-error no
OK
127.0.0.1:6379> set name neo
OK
127.0.0.1:6379> get name
"neo"

```

原因是数据库持久化写入硬盘出现问题，可能是数据库目录权限不足。

排查方法

```

CONFIG GET dir
CONFIG GET dbfilename

config set stop-writes-on-bgsave-error yes
CONFIG SET dir /tmp
CONFIG SET dbfilename temp.rdb

```

尝试解决

```

# redis-cli 
127.0.0.1:6379> CONFIG GET dir
1) "dir"
2) "/"
127.0.0.1:6379> CONFIG GET dbfilename
1) "dbfilename"
2) "dump.rdb"
127.0.0.1:6379> config set stop-writes-on-bgsave-error yes
OK
127.0.0.1:6379> CONFIG SET dir /tmp
OK
127.0.0.1:6379> set test aaaa
OK
127.0.0.1:6379> get test
"aaaa"

```

如果确认是 dir 权限问题，我们就通过修改 redis.conf 中得 dir 配置解决。