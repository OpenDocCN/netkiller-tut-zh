# 第 16 章 Apache Hbase

## 安装 Apache Hbase

注意：Apache Hbase 不能使用 OpenJDK 启动，需要去 Oracle 网站下载 Server JRE

### 单机模式安装

如果你是第一次安装 Hbase，建议你从单机安装开始，这样成功率比较高，不会受挫。Hbase 不比关系型数据库复杂，只是安装比较麻烦，一旦安装号使用起来还是很容易上手的，请直接粘贴复制下面的命令即可完成安装：

```

cd /usr/local/src
wget http://mirrors.hust.edu.cn/apache/hbase/stable/hbase-1.2.6-bin.tar.gz

tar zxf hbase-1.2.6-bin.tar.gz
cp hbase-1.2.6/conf/hbase-site.xml{,.original}
mv hbase-1.2.6 /srv/apache-hbase-1.2.6
ln -s /srv/apache-hbase-1.2.6 /srv/apache-hbase

cp /srv/apache-hbase/conf/hbase-env.sh{,.original}
cat > /srv/apache-hbase/conf/hbase-env.sh <<EOF
export JAVA_HOME=/srv/java
#export HBASE_CLASSPATH=
export HBASE_MANAGES_ZK=true
EOF

cat > /srv/hbase/conf/hbase-site.xml <<EOF
<?xml version="1.0"?>  
<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>  
<configuration>  
	<property>  
		<name>hbase.rootdir</name>  
		<value>file:///tmp/hbase-${user.name}</value>  
	</property>  
</configuration>
EOF

```

启动 Apache Hbase

```
/srv/apache-hbase/bin/start-hbase.sh

```

进入 Hbase shell

```
/srv/apache-hbase/bin/hbase shell

```

关闭 Hbase

```
/srv/apache-hbase/bin/stop-hbase.sh

```

### 伪分布式模式

单机模式基本可能满足我们的学习需要，但无法满足更复杂的需求，例如集成 Hive 等其他软件，这时我们就需要借助 Hadoop 的 HDFS 功能实现与其他软件的集成。所谓的伪分布式，就是只有一个 Hbase 节点，即 Master。

这里我假设 Hadoop 已经正确安装，无论你采用什么模式只要能提供 hdfs 服务处即可。Hadoop 安装可以参考作者的相关文档。

首先编辑 conf/hbase-site.xml 配置文件，增加以下配置:

```

<property>
  <name>hbase.cluster.distributed</name>
  <value>true</value>
</property>

```

hbase.cluster.distributed 属性值设置为 true HBase 将运行于分布式模式

然后配置 hbase.rootdir 属性值，指向 HDFS 地址。

```

<property>
  <name>hbase.rootdir</name>
  <value>hdfs://localhost:9000/hbase</value>
</property>

```

现在启动 Hbase , 如果正常使用 jps 可以下面三个线程

```

[hadoop@netkiller conf]$ su - hadoop -c "/srv/apache-hbase/bin/start-hbase.sh"
[hadoop@netkiller conf]$ jps | egrep "(HMaster|HRegionServer|HQuorumPeer)"

```

如果启动正常，你将会看到 HDFS 中的 Hbase 目录。

```

[hadoop@netkiller ~]$ /srv/hadoop/bin/hdfs dfs -ls /hbase
Found 7 items
drwxr-xr-x   - hadoop supergroup          0 2017-06-28 21:55 /hbase/.tmp
drwxr-xr-x   - hadoop supergroup          0 2017-06-28 21:55 /hbase/MasterProcWALs
drwxr-xr-x   - hadoop supergroup          0 2017-06-28 21:55 /hbase/WALs
drwxr-xr-x   - hadoop supergroup          0 2017-06-28 21:55 /hbase/data
-rw-r--r--   3 hadoop supergroup         42 2017-06-28 21:55 /hbase/hbase.id
-rw-r--r--   3 hadoop supergroup          7 2017-06-28 21:55 /hbase/hbase.version
drwxr-xr-x   - hadoop supergroup          0 2017-06-28 21:55 /hbase/oldWALs

```

### 分布式模式部署

上一节所讲的伪分不出，就是只有一个 Master 节点，而真正的分布式摸就是每个节点均独立部署，实现可伸缩，水平扩展，但作为例子这里我们仅仅采用最小化节点配置。

### 运维技巧

检查 Hbase 线程是否启动

```
[neo@netkiller conf]$ jps | grep HMaster
17719 HMaster	

```

## 配置 Apache Hbase

### hbase-env.sh

环境变量配置文件

HBASE_MANAGES_ZK=true 仅用于单机运行，true 表示不使用 Zookeeper

### hbase-site.xml

分布式模式的开启与关闭 hbase.cluster.distributed

```

<property>
  <name>hbase.cluster.distributed</name>
  <value>true</value>
</property>

```

## Hbase Shell

安装完 Apache hbase 启动后就可以进入 hbase shell 了，hbase shell 是与 Hbase 交互的界面。

```

[neo@netkiller bin]$ hbase shell
2017-06-27 21:07:35,524 WARN  [main] util.NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable
HBase Shell; enter 'help<RETURN>' for list of supported commands.
Type "exit<RETURN>" to leave the HBase Shell
Version 1.2.6, rUnknown, Mon May 29 02:25:32 CDT 2017

hbase(main):001:0>

```

退出 Hbase shell

```

hbase(main):038:0> exit
[neo@netkiller bin]$ 

```

### 表操作

首先我们做几个基本操作，例如创建表，写入数据，读取数据等等，你会发现 Hbase 被神话了，操作其实没有那么复杂，远没有关系型数据库复杂。

创建一个名为 t1 的表，使用默认命名空间 namespace=default，这个表有一个列族名(column family) 为 f1。后面会解释什么是 column family，这里你只要对着操作就可以了。

```

hbase(main):002:0> create 't1', 'f1'
0 row(s) in 1.2190 seconds

=> Hbase::Table - t1

```

向 t1 表插入数据字段名（key）是 r1,数据值 value 是 ‘value’

```

hbase(main):014:0> put 't1', 'r1', 'f1', 'value'
0 row(s) in 0.0060 seconds

```

获取表 t1 ，字段名（key）为 r1 的数据

```

hbase(main):032:0> get 't1', 'r1'
COLUMN                                              CELL                                                                                                                                                  
 f1:                                                timestamp=1498613275013, value=value                                                                                                                  
1 row(s) in 0.0240 seconds

```

列出用户表

```

hbase(main):009:0> list
TABLE                                                                                                                                                                                                     
t1                                                                                                                                                                                                      
1 row(s) in 0.0450 seconds

=> ["t1"]
hbase(main):010:0>

```

扫描表相当于 select * from t1

```

hbase(main):034:0> scan 't1'
ROW                                                 COLUMN+CELL                                                                                                                                           
 r1                                                 column=f1:, timestamp=1498613275013, value=value                                                                                                      
1 row(s) in 0.0140 seconds

```

表的启用与禁用操作

```

hbase(main):010:0> disable 't1'
0 row(s) in 1.3740 seconds

hbase(main):011:0> enable 't1'
0 row(s) in 1.2380 seconds

```

删除表，删除表之前需要先禁用该表，然后使用 drop 命令删除。

```

hbase(main):036:0> disable 't1'
0 row(s) in 2.2460 seconds

hbase(main):037:0> drop 't1'
0 row(s) in 1.2310 seconds

hbase(main):038:0>

```

## Web UI

除了 Web Shell Hbase 还提供了 Web UI 地址是：

http://localhost:16010/master-status

请确保你的防火墙放行了 16010 端口

```
[neo@netkiller conf]$ iptables-save | grep 16010
-A INPUT -p tcp -m state --state NEW -m tcp --dport 16010 -j ACCEPT

```

## Phoenix

phoenix

Phoenix 是基于 Hbase 的 SQL 层

### 安装 Phoenix

```

cd /usr/local/src/
wget https://mirrors.tuna.tsinghua.edu.cn/apache/phoenix/apache-phoenix-4.12.0-HBase-1.3/bin/apache-phoenix-4.12.0-HBase-1.3-bin.tar.gz		
tar zxvf apache-phoenix-4.12.0-HBase-1.3-bin.tar.gz 
cp apache-phoenix-4.12.0-HBase-1.3-bin/phoenix-core-4.12.0-HBase-1.3.jar /srv/apache-hbase-1.3.1/lib/
mv apache-phoenix-4.12.0-HBase-1.3-bin /srv/apache-phoenix-4.12.0
ln -s /srv/apache-phoenix-4.12.0 /srv/apache-phoenix

```

配置环境变量

```

% vim /srv/apache-hbase-1.3.1/conf/hbase-env.sh

export JAVA_HOME=/srv/java
export HBASE_CLASSPATH=/srv/apache-phoenix
export HBASE_MANAGES_ZK=true

```

环境配置

```

cat >> ~/.bash_profile << 'EOF'
export CLASSPATH=$CLASSPATH:/srv/apache-phoenix
export PATH=$PATH:/srv/apache-phoenix/bin
EOF		

```

重启 Hbase

```
su - hadoop -c "/srv/apache-hbase/bin/start-hbase.sh"

```

### sqlline.py 命令行界面

```
[hadoop@VM_7_221_centos ~]$ sqlline.py localhost		

```

```

[hadoop@VM_7_221_centos ~]$ sqlline.py 
Setting property: [incremental, false]
Setting property: [isolation, TRANSACTION_READ_COMMITTED]
issuing: !connect jdbc:phoenix:localhost:2181:/hbase none none org.apache.phoenix.jdbc.PhoenixDriver
Connecting to jdbc:phoenix:localhost:2181:/hbase
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/srv/apache-phoenix-4.12.0-HBase-1.3/phoenix-4.12.0-HBase-1.3-client.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/srv/apache-hadoop-2.8.1/share/hadoop/common/lib/slf4j-log4j12-1.7.10.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
17/10/13 10:43:48 WARN util.NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable
Connected to: Phoenix (version 4.12)
Driver: PhoenixEmbeddedDriver (version 4.12)
Autocommit status: true
Transaction isolation: TRANSACTION_READ_COMMITTED
Building list of tables and columns for tab-completion (set fastconnect to true to skip)...
95/95 (100%) Done
Done
sqlline version 1.2.0
0: jdbc:phoenix:localhost:2181:/hbase>		

```

#### 帮助信息

```

0: jdbc:phoenix:localhost> help
!all                Execute the specified SQL against all the current connections
!autocommit         Set autocommit mode on or off
!batch              Start or execute a batch of statements
!brief              Set verbose mode off
!call               Execute a callable statement
!close              Close the current connection to the database
!closeall           Close all current open connections
!columns            List all the columns for the specified table
!commit             Commit the current transaction (if autocommit is off)
!connect            Open a new connection to the database.
!dbinfo             Give metadata information about the database
!describe           Describe a table
!dropall            Drop all tables in the current database
!exportedkeys       List all the exported keys for the specified table
!go                 Select the current connection
!help               Print a summary of command usage
!history            Display the command history
!importedkeys       List all the imported keys for the specified table
!indexes            List all the indexes for the specified table
!isolation          Set the transaction isolation for this connection
!list               List the current connections
!manual             Display the SQLLine manual
!metadata           Obtain metadata information
!nativesql          Show the native SQL for the specified statement
!outputformat       Set the output format for displaying results
(table,vertical,csv,tsv,xmlattrs,xmlelements)
!primarykeys        List all the primary keys for the specified table
!procedures         List all the procedures
!properties         Connect to the database specified in the properties file(s)
!quit               Exits the program
!reconnect          Reconnect to the database
!record             Record all output to the specified file
!rehash             Fetch table and column names for command completion
!rollback           Roll back the current transaction (if autocommit is off)
!run                Run a script from the specified file
!save               Save the current variabes and aliases
!scan               Scan for installed JDBC drivers
!script             Start saving a script to a file
!set                Set a sqlline variable
!sql                Execute a SQL command
!tables             List all the tables in the database
!typeinfo           Display the type map for the current connection
!verbose            Set verbose mode on	

```

#### 创建表

```

0: jdbc:phoenix:localhost> CREATE TABLE IF NOT EXISTS us_population (
. . . . . . . . . . . . .>       state CHAR(2) NOT NULL,
. . . . . . . . . . . . .>       city VARCHAR NOT NULL,
. . . . . . . . . . . . .>       population BIGINT
. . . . . . . . . . . . .>       CONSTRAINT my_pk PRIMARY KEY (state, city));
No rows affected (2.287 seconds)

0: jdbc:phoenix:localhost> !tables 
+------------+--------------+----------------+---------------+----------+------------+----------------------------+-----------------+--------------+-----------------+---------------+---------------+---+
| TABLE_CAT  | TABLE_SCHEM  |   TABLE_NAME   |  TABLE_TYPE   | REMARKS  | TYPE_NAME  | SELF_REFERENCING_COL_NAME  | REF_GENERATION  | INDEX_STATE  | IMMUTABLE_ROWS  | SALT_BUCKETS  | MULTI_TENANT  | V |
+------------+--------------+----------------+---------------+----------+------------+----------------------------+-----------------+--------------+-----------------+---------------+---------------+---+
|            | SYSTEM       | CATALOG        | SYSTEM TABLE  |          |            |                            |                 |              | false           | null          | false         |   |
|            | SYSTEM       | FUNCTION       | SYSTEM TABLE  |          |            |                            |                 |              | false           | null          | false         |   |
|            | SYSTEM       | SEQUENCE       | SYSTEM TABLE  |          |            |                            |                 |              | false           | null          | false         |   |
|            | SYSTEM       | STATS          | SYSTEM TABLE  |          |            |                            |                 |              | false           | null          | false         |   |
|            |              | US_POPULATION  | TABLE         |          |            |                            |                 |              | false           | null          | false         |   |
+------------+--------------+----------------+---------------+----------+------------+----------------------------+-----------------+--------------+-----------------+---------------+---------------+---+

```

### SQuirreL SQL Client

[`www.squirrelsql.org/`](http://www.squirrelsql.org/)

## FAQ

### HBaseConfTool : Unsupported major.minor version 51.0

错误提示

```
Exception in thread "main" java.lang.UnsupportedClassVersionError: org/apache/hadoop/hbase/util/HBaseConfTool : Unsupported major.minor version 51.0			

```

解决方案，Hbase 不支持 OpenJDK 更换 Oracle Server JRE 后可以解决。

### ignoring option PermSize=128m; support was removed in 8.0

jvm 1.8 之后不再支持 PermSize 和 MaxPermSize 选项

```
[neo@netkiller hbase]$ bin/start-hbase.sh
starting master, logging to /srv/hbase/bin/../logs/hbase-root-master-localhost.localdomain.out
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option PermSize=128m; support was removed in 8.0
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=128m; support was removed in 8.0

```

解决方案："-server -Xms2048m -Xmx4096m"