# 附录 B. 历史记录

| **修订历史** |
| 修订 1.0 | 2007-1-12 |   |
|  
*   开始

*   ubuntu linux

 |
| 修订 1.1 | 2007-5-10 |   |
| Application (Zope) |
| 修订 1.2 | 2007-5-15 |   |
| Memcached |
| 修订 1.3 | 2007-5-18 |   |
| Jboss |
| 修订 1.4 | 2007-5-21 |   |
| php memcache,lighttpd script |
| 修订 1.5 | 2007-5-22 |   |
| rsync |
| 修订 1.6 | 2007-5-24 |   |
| openfiler |
| 修订 1.7 | 2007-5-25 |   |
| openfiler, php sql server |
| 修订 1.8 | 2007-5-28 |   |
| openfiler, zend optimizer |
| 修订 1.9 | 2007-6-9 |   |
| ip tunnel, memcached script, lighttpd script |
| 修订 1.10 | 2007-11-13 |   |
| 栏目重新排版，增加很多新内容 |
| 修订 1.11 | 2008-1-17 |   |
| awstats, webalizer |
| 修订 1.12 | 2008-1-22 |   |
| TUTOS, TRAC |
| 修订 1.2 | 2008-3-21 |   |
| 栏目重新排版，增加很多新内容 |
| 修订 1.2.1 | 2008-3-21 |   |
| Shorewall |
| 修订 1.2.2 | 2008-6-20 |   |
| FreeRADIUS |
| 修订 1.2.3 | 2008-10-7 |   |
| MySQL Replication |
| 修订 1.2.4 | 2008-10-8 |   |
| MySQL Cluster |
| 修订 1.2.5 | 2008-10-9 |   |
| modi: Openldap |
| 修订 1.2.6 | 2008-10-21 |   |
| ufw - program for managing a netfilter firewallinotify-toolsDRBD (Distributed Replicated Block Device) |
| 修订 1.2.7 | 2008-10-31 |   |
| modify rsync chapteradd csync2 |
| 修订 1.2.8 | 2008-12-3 |   |
| modified system chapteradd nagios, and remove developer chapter |
| 修订 1.2.9 | 2008-12-16 |   |
| the system chapter was modified |
| 修订 1.2.10 | 2008-12-22 |   |
| added loop devicesadded ACL - Access Control List under chapter security.added ncftp, ncftpget, ncftpput |
| 修订 1.3.0 | 2009-3-10 |   |
| bashadded if, for, while, untiland function |
| 修订 1.3.1 | 2009-3-22 |   |
| vsftpd |
| 修订 1.3.2 | 2009-4-5 |   |
| to move chapter database to new docbook. |
| 修订 1.3.2 | 2009-4-15 |   |
| Stunnel. |
| 修订 1.3.3 | 2009-5-7 |   |
| 增加很多新内容,章节重新排版。 |
| 修订 1.3.4 | 2009-10-27 |   |
| PPTPD |
| 修订 1.3.4 | 2012-01-01 |   |
| sv - control and manage services monitored by runsv |