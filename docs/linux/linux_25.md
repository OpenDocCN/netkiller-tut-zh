# 附录 A. 附录

## 1. 贡献用户列表

刘军 QQ: 470499989, 470499989@qq.com

## 2. 参考文档

http://www.faqs.org/docs/Linux-HOWTO/Bash-Prog-Intro-HOWTO.html

http://xiaowang.net/bgb-cn/index.html

## 3. Red Hat 漏洞

下面链接是 Red Hat 公布的漏洞，你需要格外留意这些漏洞。

[Red Hat vulnerabilities by CVE name](https://access.redhat.com/security/cve/)

不一定每个漏洞都威胁到你的安全，所以升级要试情况而定，每次升级都存在一定风险，尤其是对于你手工编译的软件。

## 4. National Vulnerability Database (NVD)

美国国家漏洞数据库

[点击进入](https://web.nvd.nist.gov/view/vuln/search-results)

## 5. Common Vulnerabilities and Exposures

[`cve.mitre.org/index.html`](https://cve.mitre.org/index.html)

## 6. Red Hat Bug 平台

[`bugzilla.redhat.com/`](https://bugzilla.redhat.com/)

## 7. Redhat Doc

[`docs.redhat.com/docs/zh-CN/index.html`](http://docs.redhat.com/docs/zh-CN/index.html)

## 8. System reduce

采用最小化安装后仍有很多不必要的软件

```
			[root@dev1 ~]# yum remove cups

```