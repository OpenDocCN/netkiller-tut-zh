# Netkiller Blockchain 手札

## 本文作者最近在找工作，有意向致电 13113668890

ISBN# 

### Mr. Neo Chan, 陈景峯(BG7NYT)

中国广东省深圳市望海路半岛城邦三期
518067
+86 13113668890

`<netkiller@msn.com>`

电子书最近一次更新于 2020-04-10 18:48:07 .

版权 © 2018-2020 Netkiller(Neo Chan). All rights reserved.

**版权声明**

转载请与作者联系，转载时请务必标明文章原始出处和作者信息及本声明。

|  &#124; ![ &#124;](https://creativecommons.org/licenses/by/4.0/)  |  

&#124; [`www.netkiller.cn`](http://www.netkiller.cn) &#124;
&#124; [`netkiller.github.io`](http://netkiller.github.io/) &#124;
&#124; [`netkiller.sourceforge.net`](http://netkiller.sourceforge.net/) &#124;

 |  &#124; ![ &#124;](img/weixin.jpg)  |  

&#124; 微信订阅号 netkiller-ebook (微信扫描二维码） &#124;
&#124; QQ：13721218 请注明“读者” &#124;
&#124; QQ 群：128659835 请注明“读者” &#124;

 |

内容摘要

这一部关于区块链开发及运维的电子书。

为什么会写区块链电子书？因为 2018 年是区块链年，区块链是一个风口，前几个风口我都错过了。例如 web2.0, 云， 大数据等等，都从身旁擦肩而过。所以我要抓住这次。

这本电子书是否会出版（纸质图书）？ 不会，因为互联网技术更迭太快，纸质书籍的内容无法实时更新，一本书动辄百元，很快就成为垃圾，你会发现目前市面的上区块链书籍至少是一年前写的，内容已经过时，很多例子无法正确运行。所以我不会出版，电子书的内容会追逐技术发展，及时跟进软件版本的升级，做到内容最新，至少是主流。

这本电子书与其他区块链书籍有什么不同？市面上大部分区块链书籍都是用 2/3 去讲区块链原理，只要不到 1/3 的干货，干货不够理论来凑，通篇将理论或是大谈特谈区块链行业，这些内容更多是头脑风暴，展望区块链，均无法落地实施。本书与那些书籍完全不同，不讲理论和原理，面向应用落地，注重例子，均是干货。

写作原则，无法落地的项目作者绝对不会写。凡是写入电子的内容均具备可操作，可落地。

电子书更新频率？每天都会有新内容加入，更新频率最迟不会超过一周，更新内容请关注 [`github.com/netkiller/netkiller.github.io/commits/master`](https://github.com/netkiller/netkiller.github.io/commits/master)

本文采用碎片化写作，原文会不定期更新，请尽量阅读原文。 [`www.netkiller.cn/blockchain/index.html`](http://www.netkiller.cn/blockchain/index.html)

您的打赏是我的写作动力： [`www.netkiller.cn/blockchain/donations.html`](http://www.netkiller.cn/blockchain/donations.html)

接受 ETH 捐赠： [0x3e827461Cc53ed7c75A29187CfF39629FCAE3661](https://etherscan.io/address/0x3e827461Cc53ed7c75A29187CfF39629FCAE3661)

喜大普奔！读者币（Netkiller eBook Reader Coin - NBRC）正式开始空投，请在钱包中添加 NBRC 代币即可看到 1000 枚读者币。建议使用 Ethereum Wallet (Mist) 操作 NBRC，在 CONTRACTS 菜单点击 WATCH TOKEN 按钮，输入合约地址 0x4488ed050cd13ccfe0b0fcf3d168216830142775 。注意 imtoken 计算 Gas 有问题，转账会失败。MetaMask 建议 Gas Price 4GWei, Gas Limit 200000 费用 0.0004 左右的 ETH

表 1. 企业招聘信息广告位，区块链工作机会

|  |  |
| --- | --- |
|  |  |
|  |  |

|  &#124; ![ &#124;](http://www.yungeen.cn)  |  | LOGO 广告位招租 | LOGO 广告位招租 | LOGO 广告位招租 |

广告发布，请联系 13113668890

* * *

# 致读者

Netkiller 系列电子书始于 2000 年，风风雨雨走过 20 年，将在 2020 年终结，之后不在更新。作出这种决定原因很多，例如现在的阅读习惯已经转向短视频，我个人的时间，身体健康情况等等......

感谢读者粉丝这 20 年的支持

虽然电子书不再更新，后面我还会活跃在知乎社区和微信公众号