# Netkiller Java 手札

## Java, Servlet, JavaBean, Struts, Spring ...

ISBN# 

### Mr. Neo Chan, 陈景峯(BG7NYT)

中国广东省深圳市望海路半岛城邦三期
518067
+86 13113668890

`<netkiller@msn.com>`

文档始创于 2015-11-10 

电子书最近一次更新于 2020-04-10 05:18:48 .

版权 © 2015-2019 Netkiller(Neo Chan). All rights reserved.

**版权声明**

转载请与作者联系，转载时请务必标明文章原始出处和作者信息及本声明。

|  &#124; ![ &#124;](http://creativecommons.org/licenses/by/3.0/)  |  

&#124; [`www.netkiller.cn`](http://www.netkiller.cn) &#124;
&#124; [`netkiller.github.io`](http://netkiller.github.io/) &#124;
&#124; [`netkiller.sourceforge.net`](http://netkiller.sourceforge.net/) &#124;

 |  &#124; ![ &#124;](img/weixin.jpg)  |  

&#124; 微信订阅号 netkiller-ebook (微信扫描二维码） &#124;
&#124; QQ：13721218 请注明“读者” &#124;
&#124; QQ 群：128659835 请注明“读者” &#124;

 |

2017-11

关于《Netkiller Java 手札》

作者 2002 年开始在项目中使用 Java，各种原因没有留下 Java 文档，2015 因工作需要重新拾起 Java 并整理本文档。

本电子书重点内容是 Spring boot, Spring cloud, Spring data, Spring security

我的系列文档

编程语言

| Netkiller Architect 手札 | Netkiller Developer 手札 | Netkiller Java 手札 | Netkiller Spring 手札 | Netkiller PHP 手札 | Netkiller Python 手札 |
| Netkiller Testing 手札 | Netkiller Cryptography 手札 | Netkiller Perl 手札 | Netkiller Docbook 手札 | Netkiller Project 手札 | Netkiller Database 手札 |

* * *

# 致读者

Netkiller 系列电子书始于 2000 年，风风雨雨走过 20 年，将在 2020 年终结，之后不在更新。作出这种决定原因很多，例如现在的阅读习惯已经转向短视频，我个人的时间，身体健康情况等等......

感谢读者粉丝这 20 年的支持

虽然电子书不再更新，后面我还会活跃在知乎社区和微信公众号